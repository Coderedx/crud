package com.example.crud.controller;

import com.example.crud.dto.BuyerDto;
import com.example.crud.dto.PathDto;
import com.example.crud.service.BuyerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/buyer")
@RequiredArgsConstructor
public class BuyerController {

    private final BuyerService buyerService;

    @GetMapping("/search")
    public ResponseEntity<BuyerDto> search(@RequestBody PathDto pathDto) throws IOException {
        return ResponseEntity.ok(buyerService.search(pathDto));
    }
}

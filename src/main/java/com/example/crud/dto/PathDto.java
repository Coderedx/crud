package com.example.crud.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PathDto {
    private String inputPath;
    private String outputPath;
}

package com.example.crud.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuyerDto {
    private String lastName;
    private String firstName;
}

package com.example.crud.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class JsonFile {
    private List<CriteriaDto> criterias;
}

package com.example.crud.util;

import com.example.crud.dto.BuyerDto;
import com.example.crud.dto.JsonFile;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.List;

@Component
@RequiredArgsConstructor
public class JsonHelper {

    private final ObjectMapper mapper;

    public JsonFile parse(String path) throws IOException {
        InputStream is = JsonFile.class.getResourceAsStream("/data.json");
        return mapper.readValue(is, JsonFile.class);
    }

    public void write(String path, List<BuyerDto> result) throws IOException {
        mapper.writeValue(Paths.get(path).toFile(), result);
    }
}

package com.example.crud.service;

import com.example.crud.dto.BuyerDto;
import com.example.crud.dto.JsonFile;
import com.example.crud.dto.PathDto;
import com.example.crud.util.JsonHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class BuyerService {

    private final JsonHelper jsonHelper;
    public BuyerDto search(PathDto pathDto) throws IOException {
        JsonFile json = jsonHelper.parse(pathDto.getInputPath());
        BuyerDto result = new BuyerDto();
        result.setFirstName("Марк");
        result.setLastName("Цукерберг");
        List<BuyerDto> results = new ArrayList<>();
        results.add(result);
        jsonHelper.write(pathDto.getOutputPath(), results);
        return result;
    }
}
